import {createStore} from 'vuex';
import basket from 'scripts/model/modules/basket';
import favorite from 'scripts/model/modules/favorite';
import user from 'scripts/model/modules/user';

const store = createStore({
	modules: {
		basket,
		favorite,
		user
	}
});

export default store;