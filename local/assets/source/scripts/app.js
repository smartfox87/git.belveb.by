import {createApp} from 'vue';
import store from 'scripts/model/model';
import components from 'scripts/components';
import { maska } from 'maska'; // https://github.com/beholdr/maska

const createAppExt = (el) => {
	let app = createApp({});

	app.use(store);
	app.directive('maska', maska);

	for (let name in components) {
		app.component(name, components[name]);
	}

	if (el) {
		app.mount(el);
	}
	
	return app;
};

export {createAppExt};