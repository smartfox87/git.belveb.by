export default function uniqueId(prefix = '') {
	return prefix + Date.now().toString() + Math.round(Math.random() * 100000);
}